import { AuthGuard } from './auth.guard';
import { FrontPageComponent } from './front-page/front-page.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProfilComponent } from './profil/profil.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo:'/front', pathMatch:'full'
  },
  {
    path: 'front',
    component: FrontPageComponent,
  },
  {
    path: 'profil',
    component: ProfilComponent,
    canActivate: [AuthGuard]

  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [FrontPageComponent, ProfilComponent, RegisterComponent, LoginComponent]
