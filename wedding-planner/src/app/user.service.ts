import { User } from './user';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class UserService {
    private _getUrl= "/api/users/";
    constructor(private _http:Http ){ }
 
    getUser(){
      return this._http.get(this._getUrl)
        .pipe(map((response: Response) => response.json()))
    }

   
 
}
