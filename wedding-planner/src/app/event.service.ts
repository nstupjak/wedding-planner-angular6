import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class EventService {
  private _frontUrl = "http://localhost:3000/api/front"
  private _profileUrl = "http://localhost:3000/api/profil"
  
  
  constructor(private http: HttpClient) { }

  getProfile(){
    return this.http.get<any>(this._profileUrl)
  }

  getFront(){
    return this.http.get<any>(this._frontUrl)
  }

}
