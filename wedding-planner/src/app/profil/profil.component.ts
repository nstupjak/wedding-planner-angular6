import { UserService } from './../user.service';
import { User } from './../user';
import { Router } from '@angular/router';
import { EventService } from './../event.service';
import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import * as $ from 'jQuery'



@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css'],
  providers: [UserService]
})

export class ProfilComponent implements OnInit {
  profil=[ ]

  users: Array<User>
  selectedUser: User;

  constructor(private _userService: UserService, private _eventService: EventService, private _router: Router) { }

  ngOnInit() {
    this._eventService.getProfile()
      .subscribe(
        res => this.profil = res,
        err => {
          if( err instanceof HttpErrorResponse ) {
            if (err.status === 401) {
              this._router.navigate(['/login'])
            }
          }
        }
      )
     
    this._userService.getUser()
        .subscribe(resUserData => {
          var x
          for(x in resUserData){
            if (resUserData[x].userName == localStorage.getItem("userName")){
              this.selectedUser = resUserData[x]
            }
          }
        }
      )

  }
mBurme(value2){
  $.ajax({
    url: 'api/users/'+ localStorage.getItem("userName"),
    type: 'PUT',
    data: {burme:value2}
  });
  this.selectedUser.burme=value2;
 }
 mMaticar(value2){
  $.ajax({
    url: 'api/users/'+ localStorage.getItem("userName"),
    type: 'PUT',
    data: {maticar:value2}
  });
  this.selectedUser.maticar=value2;
 }

 mCrkva(value2){
  $.ajax({
    url: 'api/users/'+ localStorage.getItem("userName"),
    type: 'PUT',
    data: {crkva:value2}
  });
  this.selectedUser.crkva=value2;
 }

 mPjevaci(value2){
  $.ajax({
    url: 'api/users/'+ localStorage.getItem("userName"),
    type: 'PUT',
    data: {pjevaci:value2}
  });
  this.selectedUser.pjevaci=value2;
 }

 mBuket(value2){
  $.ajax({
    url: 'api/users/'+ localStorage.getItem("userName"),
    type: 'PUT',
    data: {buket:value2}
  });
  this.selectedUser.buket=value2;
 }

 mVjencanica(value2){
  $.ajax({
    url: 'api/users/'+ localStorage.getItem("userName"),
    type: 'PUT',
    data: {vjencanica:value2}
  });
  this.selectedUser.vjencanica=value2;
 }

mCipeleMlada(value2){
  $.ajax({
    url: 'api/users/'+ localStorage.getItem("userName"),
    type: 'PUT',
    data: {cipeleMlada:value2}
  });
  this.selectedUser.cipeleMlada=value2;
 }

 mSminka(value2){
  $.ajax({
    url: 'api/users/'+ localStorage.getItem("userName"),
    type: 'PUT',
    data: {sminka:value2}
  });
  this.selectedUser.sminka=value2;
 }

 mFrizura(value2){
  $.ajax({
    url: 'api/users/'+ localStorage.getItem("userName"),
    type: 'PUT',
    data: {frizura:value2}
  });
  this.selectedUser.frizura=value2;
 }

 mOdijelo(value2){
  $.ajax({
    url: 'api/users/'+ localStorage.getItem("userName"),
    type: 'PUT',
    data: {odijelo:value2}
  });
  this.selectedUser.odijelo=value2;
 }

 mcipeleMladoz(value2){
  $.ajax({
    url: 'api/users/'+ localStorage.getItem("userName"),
    type: 'PUT',
    data: {cipeleMladoz:value2}
  });
  this.selectedUser.cipeleMladoz=value2;
 }
 mhrana(value2){
  $.ajax({
    url: 'api/users/'+ localStorage.getItem("userName"),
    type: 'PUT',
    data: {hrana:value2}
  });
  this.selectedUser.hrana=value2;
 }

 mSviraci(value2){
  $.ajax({
    url: 'api/users/'+ localStorage.getItem("userName"),
    type: 'PUT',
    data: {sviraci:value2}
  });
  this.selectedUser.sviraci=value2;
 }

 mDekoracije(value2){
  $.ajax({
    url: 'api/users/'+ localStorage.getItem("userName"),
    type: 'PUT',
    data: {dekoracije:value2}
  });
  this.selectedUser.dekoracije=value2;
 }
 mSala(value2){
  $.ajax({
    url: 'api/users/'+ localStorage.getItem("userName"),
    type: 'PUT',
    data: {sala:value2}
  });
  this.selectedUser.sala=value2;
 }

 mBend(value2){
  $.ajax({
    url: 'api/users/'+ localStorage.getItem("userName"),
    type: 'PUT',
    data: {bend:value2}
  });
  this.selectedUser.bend=value2;
 }

 mReveri(value2){
  $.ajax({
    url: 'api/users/'+ localStorage.getItem("userName"),
    type: 'PUT',
    data: {reveri:value2}
  });
  this.selectedUser.reveri=value2;
 }
 mPozivnice(value2){
  $.ajax({
    url: 'api/users/'+ localStorage.getItem("userName"),
    type: 'PUT',
    data: {pozivnice:value2}
  });
  this.selectedUser.pozivnice=value2;
 }
 mZahvalnice(value2){
  $.ajax({
    url: 'api/users/'+ localStorage.getItem("userName"),
    type: 'PUT',
    data: {zahvalnice:value2}
  });
  this.selectedUser.zahvalnice=value2;
 }
}


