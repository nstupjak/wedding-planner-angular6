import { EventService } from './../event.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-front-page',
  templateUrl: './front-page.component.html',
  styleUrls: ['./front-page.component.css']
})
export class FrontPageComponent implements OnInit {

  front = []
  constructor(private _auth: AuthService, private _eventService: EventService ) { }

  ngOnInit() {
    this._eventService.getProfile()
    .subscribe(
      res => this.front = res,
      err => console.log(err)
    )
  }

}
