const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const path = require('path')

const PORT = 3000
const api = require('./routes/api')
const app = express()
app.use(cors())

app.use(bodyParser.json())
app.use(express.static(path.join(__dirname, '../wedding-planner/dist/wedding-planner')))
app.use(bodyParser.urlencoded({extended: true}))

app.use('/api', api)
app.get('/', function(req, res) {
    res.send('server')
})
app.get('*', (req,res) => {
    res.sendFile(path.json(__dirname, './wedding-planner/dist/wedding-planner/index.html'))
})
app.listen(PORT, function(){
    console.log("Server running on localhost:"+ PORT)
})

