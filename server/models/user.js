const mongoose = require('mongoose')

const Schema = mongoose.Schema
const userSchema = new Schema({
    email: String,
    password: String, 
    userName: String,
    cpassword: String,
    firstName: String,
    lastName: String,
    pName: String,
    budzet: Number,
    obredD: Date,
    burme: Boolean,
    maticar: Boolean,
    crkva: Boolean,
    pjevaci: Boolean,
    buket: Boolean,
    vjencanica: Boolean,
    cipeleMlada: Boolean,
    sminka: Boolean,
    frizura: Boolean,
    odijelo: Boolean,
    cipeleMladoz: Boolean,
    hrana: Boolean,
    sviraci: Boolean,
    dekoracije: Boolean,
    sala: Boolean,
    bend: Boolean,
    reveri: Boolean,
    pozivnice: Boolean,
    zahvalnice: Boolean,
    fotograf: Boolean,

})

module.exports =  mongoose.model('user', userSchema, 'users')