const express = require('express')
const jwt = require('jsonwebtoken')
const router = express.Router()
const User = require('../models/user')

const mongoose = require('mongoose')
const db = ""
mongoose.Promise = global.Promise;

mongoose.connect(db, err => {
    if (err){
        console.error('Error!' + err)
    } else
    console.log('Connected to mongodb')
})

function verifyToken(req, res, next) {
    if(!req.headers.authorization) {
      return res.status(401).send('Unauthorized request')
    }
    let token = req.headers.authorization.split(' ')[1]
    if(token === 'null') {
      return res.status(401).send('Unauthorized request')    
    }
    let payload = jwt.verify(token, 'secretKey')
    if(!payload) {
      return res.status(401).send('Unauthorized request')    
    }
    req.userId = payload.subject
    next()
  }


router.get('/', (req, res) => {
    res.send('From API route')
})

router.post('/register', (req, res) =>{
    let userData = req.body
    let user = new User(userData)
    user.save((error, registeredUser) => {
        if (error) {
            console.log(error)
        } else {
            let payload = { subject : registeredUser._id}
            let token = jwt.sign(payload, 'secretKey')
            res.status(200).send({token})
        }
    })
})

router.post('/login', (req, res) => {
    let userData = req.body

    User.findOne({userName: userData.userName}, (error, user) => {
        if (error){
            console.log(error)
        } else {
            if(!user) {
                res.status(401).send('Invalid username')
            } else 
            if (user.password !== userData.password) {
                res.status(401).send('Invalid password')
            } else {
                let payload = { subject: user._id}
                let token = jwt.sign(payload, 'secretKey')
                res.status(200).send({token})
            }
        }
    })

})

router.post('/profil', verifyToken, (req, res) => {
    let profil = []
    res.json(profil)

})

router.get('/front', (req, res) => {
    let front =[]
    res.json(front)

})
router.get('/create', (req, res) => {
    let create =[]
    res.json(create)

})

router.get('/users', function(req,res){
    User.find({})
    .exec(function(err, users){
        if(err){
            console.log("Error retrieving user")
        } else {
            res.json(users);
        }
    });
});

router.get('/users/:userName', function(req,res){
    User.findOne({userName: req.params.userName})
    .exec(function(err, user){
        if(err){
            console.log("Error retrieving users")
        } else {
            res.json(user);
        }
    });
});


router.put('/users/:userName', function(req, res){
    User.findOneAndUpdate({userName: req.params.userName},
    {
        $set: req.body
    }, 
    {
        new: true
    },
        function(err, updatedUser) {
            if(err){
                res.send("Error updating user")
            } else{
                res.json(updatedUser);
            }
        }
    );

});

router.delete('video/:id', function(req,res){
    console.log('Deleting a user');
    User.findByIdAndRemove(req.params.id, function(err, deletedUser){
        if(err){
            res.send("Error deleting user");
        }else{
            res.json(deletedUser);
        }
    });
});

module.exports = router